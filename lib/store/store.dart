import 'package:language_helper_flutter/middleware/middleware.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/reducers/app_reducer.dart';
import 'package:redux/redux.dart';

Store<AppState> createStore() {
  Store<AppState> store = new Store(
    appReducer,
    initialState: new AppState(),
    middleware: createMiddleware(),
  );
  persistor.load(store);

  return store;
}

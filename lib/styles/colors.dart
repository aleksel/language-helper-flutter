import 'package:flutter/material.dart';

Map<String, dynamic> appColorStyles = {
  'primary_dark': const Color.fromRGBO(31, 34, 39, 1.0),
  'primary': const Color.fromRGBO(64, 88, 104, 1.0),
  'primary_dasabled': const Color.fromRGBO(64, 88, 104, 0.65),
  'primary_row': const Color.fromRGBO(225, 225, 225, 0.6),
  'primary_material': Colors.blueGrey,
  'ligth_font': Colors.black54,''
  'gray': Colors.black45,
  'black': Colors.black,
  'black60': const Color.fromRGBO(0, 0, 0, 0.60),
  'black80': const Color.fromRGBO(0, 0, 0, 0.80),
  'white': Colors.white,
  'white30': Colors.white30,
  'white50': const Color.fromRGBO(255, 255, 255, 0.50),
  'white70': Colors.white70,
  'white80': const Color.fromRGBO(255, 255, 255, 0.80),
  'white90': const Color.fromRGBO(255, 255, 255, 0.90),
  'white40_func': () => const Color.fromRGBO(255, 255, 255, 0.40),
  'white90_func': () => const Color.fromRGBO(255, 255, 255, 0.90),
};

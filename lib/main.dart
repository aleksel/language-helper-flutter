import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:language_helper_flutter/middleware/middleware.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/presentation/platform_adaptive.dart';
import 'package:language_helper_flutter/screens/empty_screen.dart';
import 'package:language_helper_flutter/screens/login_screen.dart';
import 'package:language_helper_flutter/screens/main_screen.dart';
import 'package:language_helper_flutter/store/store.dart';
import 'package:language_helper_flutter/utils/custom_route.dart';
import 'package:redux/redux.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

void main() {
  //HttpOverrides.global = new StethoHttpOverrides();
  final store = createStore();
  runApp(new LanguageApp(store));
}

class LanguageApp extends StatelessWidget {
  LanguageApp(this.store);

  final Store<AppState> store;

  @override
  Widget build(BuildContext context) {

    return new PersistorGate(
        persistor: persistor,
        //loading: new LoadingScreen(),
        builder: (context) => new StoreProvider<AppState>(
            store: store,
            child: new MaterialApp(
                title: 'ReduxApp',
                theme: defaultTargetPlatform == TargetPlatform.iOS ? kIOSTheme : kDefaultTheme,
                onGenerateRoute: (RouteSettings settings) {
                  switch (settings.name) {
                    case '/':
                      return new CustomRoute(
                          builder: (BuildContext context) => new StoreConnector<AppState, dynamic>(
                                converter: (store) => store.state.auth.isAuthenticated,
                                builder: (BuildContext context, isAuthenticated) =>
                                    isAuthenticated ? new MainScreen() : new LoginScreen(),
                              ));
                    case '/login':
                      return new CupertinoPageRoute(builder: (BuildContext context) => new LoginScreen());
                    case '/main':
                      return new CupertinoPageRoute(builder: (BuildContext context) => new MainScreen());
                    case '/empty':
                      return new CupertinoPageRoute(builder: (BuildContext context) => new EmptyScreen());
                  }
                })));
  }
}

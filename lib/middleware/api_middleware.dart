import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/utils/http_utils.dart';
import 'package:language_helper_flutter/utils/text_constatnt.dart';
import 'package:redux/redux.dart';

abstract class Promise {
  Promise(this.promise, {this.failureAction, this.successAction, this.failureHandler, this.successHandler});

  Function promise;
  Function failureAction;
  Function successAction;
  Function failureHandler;
  Function successHandler;
}

class ActionFailure {
  final String errorMessage;
  final Map<String, dynamic> details;

  ActionFailure(this.errorMessage, [this.details]);
}

final Function failureAction = (store, objectFactory) => (data) {
      if (data.statusCode == 400) {
        if (data.response['details'] != null || data.response['errorMessage'] != null) {
          store.dispatch(objectFactory(data.response['errorMessage'], data.response['details']));
        }
      } else if (data.response['errorMessage'] != null) {
        store.dispatch(objectFactory(data.response['errorMessage']));
      } else {
        store.dispatch(objectFactory(httpSystemError, new Map<String, dynamic>()));
      }
    };

createApiMiddleware() => (Store<AppState> store, dynamic action, NextDispatcher next) {
      if (action is Promise) {
        action.promise().then((data) {
          if (data.statusCode > 299 || data.statusCode < 200) {
            if (action.failureHandler != null) {
              if (action.failureHandler(data)) return;
            }
            store.dispatch(action.failureAction(data));
          } else {
            if (action.successHandler != null) {
              if (action.successHandler(data)) return;
            }
            store.dispatch(action.successAction(data));
          }
        });
      }
      next(action);
    };

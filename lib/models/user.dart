import 'dart:convert';

class User {
  final String token;
  String email;
  String phone;
  String firstName;
  String lastName;
  int exp;

  User([this.token]) {
    if (this.token != null) {
      final List<String> parts = this.token.split(".");
      final String payloadString = _decodeBase64(parts[1]);
      final map = json.decode(payloadString);
      this.email = map['email'] ?? '';
      this.phone = map['phone'] ?? '';
      this.firstName = map['firstName'] ?? '';
      this.lastName = map['lastName'] ?? '';
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{'token': this.token};

  factory User.fromJson(Map<String, dynamic> json) => new User(
        json['token'],
      );

  @override
  String toString() {
    return '{token: $token}';
  }
}

String _decodeBase64(String str) {
  String output = str.replaceAll('-', '+').replaceAll('_', '/');

  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw 'Illegal base64url string!"';
  }

  return utf8.decode(base64Url.decode(output));
}

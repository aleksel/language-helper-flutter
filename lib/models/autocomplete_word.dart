class AutocompleteWord {
  final String word;
  final String mainForm;

  AutocompleteWord({this.word, this.mainForm});

  Map<String, dynamic> toJSON() => <String, dynamic>{'word': this.word, 'main_form': this.mainForm};

  factory AutocompleteWord.fromJSON(Map<String, dynamic> json) => new AutocompleteWord(
    word: json['word'], mainForm: json['main_form'],
  );

  @override
  String toString() {
    return '{word: $word, mainForm: $mainForm}';
  }
}

class UserSuggestion {
  final String word;
  final String infinitive;
  final String highlight;

  UserSuggestion(this.word, this.infinitive, this.highlight) : assert(word != null);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'word': this.word,
        'infinitive': this.infinitive,
        'highlight': this.highlight,
      };

  factory UserSuggestion.fromJson(Map<String, dynamic> json) => new UserSuggestion(
        json['word'],
        json['infinitive'],
        json['highlight'],
      );

  @override
  String toString() {
    return '''
    {
      word: $word,
      infinitive: $infinitive,
      highlight: $highlight,
    }
    ''';
  }
}

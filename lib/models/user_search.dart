class UserSearch {
  final String searchText;
  final int count;
  final DateTime createdAt;
  final DateTime updatedAt;

  UserSearch(this.searchText, this.count, this.createdAt, this.updatedAt) : assert(searchText != null);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'searchText': this.searchText,
        'count': this.count,
        'createdAt': this.createdAt == null ? null : this.createdAt.toIso8601String(),
        'updatedAt': this.updatedAt == null ? null : this.updatedAt.toIso8601String(),
      };

  factory UserSearch.fromJson(Map<String, dynamic> json) => new UserSearch(
        json['searchText'],
        json['count'],
        DateTime.parse(json['createdAt']),
        DateTime.parse(json['updatedAt']),
      );

  @override
  String toString() {
    return '''
    {
      searchText: $searchText,
      count: $count,
      createdAt: $createdAt,
      updatedAt: $updatedAt,
    }
    ''';
  }
}

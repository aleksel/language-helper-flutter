import 'dart:convert';

import 'package:language_helper_flutter/models/user.dart';
import 'package:language_helper_flutter/models/user_search.dart';
import 'package:meta/meta.dart';

@immutable
class UserSearchState {
  // properties
  final bool isFetched;
  final bool isLoading;
  final List<UserSearch> userSearches;
  final String errorMessage;
  final Map<String, dynamic> errorDetails;

  // constructor with default
  UserSearchState({
    this.isFetched = false,
    this.isLoading = false,
    this.userSearches,
    this.errorMessage,
    this.errorDetails,
  });

  // allows to modify UserSearchState parameters while cloning previous ones
  UserSearchState copyWith(
      {bool isFetched,
      bool isLoading,
      String errorMessage,
      Map<String, dynamic> errorDetails,
      List<UserSearch> userSearches}) {
    return new UserSearchState(
      isFetched: isFetched ?? this.isFetched,
      isLoading: isLoading ?? this.isLoading,
      errorMessage: errorMessage ?? this.errorMessage,
      errorDetails: errorDetails ?? this.errorDetails,
      userSearches: userSearches ?? this.userSearches,
    );
  }

  factory UserSearchState.fromJSON(Map<String, dynamic> json) {
    List<UserSearch> userSearches = new List<UserSearch>();
    if (json['userSearches'] != null) {
      List<dynamic> response = json['userSearches'];
      response.forEach((item) => userSearches.add(new UserSearch.fromJson(item)));
    }
    return new UserSearchState(
      isFetched: false,
      isLoading: false,
      errorMessage: json['errorMessage'],
      errorDetails: json['errorDetails'],
      userSearches: userSearches,
    );
  }

  Map<String, dynamic> toJSON() => <String, dynamic>{
        'isFetched': this.isFetched,
        'isLoading': this.isLoading,
        'userSearches': this.userSearches == null ? new List() : this.userSearches,
        'errorMessage': this.errorMessage,
        'errorDetails': this.errorDetails,
      };

  @override
  String toString() {
    return '''{
                isFetched: $isFetched,
                isLoading: $isLoading,
                userSearches: $userSearches
                errorMessage: $errorMessage
                errorDetails: $errorDetails
            }''';
  }
}

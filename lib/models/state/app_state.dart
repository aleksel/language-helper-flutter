import 'package:language_helper_flutter/models/state/auth_state.dart';
import 'package:language_helper_flutter/models/state/user_search_state.dart';
import 'package:language_helper_flutter/models/state/user_suggestion_state.dart';
import 'package:meta/meta.dart';

@immutable
class AppState {
  final AuthState auth;
  final UserSearchState userSearch;
  final UserSuggestionState userSuggestion;

  AppState({AuthState auth, UserSearchState userSearch, UserSuggestionState userSuggestion})
      : auth = auth ?? new AuthState(),
        userSearch = userSearch ?? new UserSearchState(userSearches: new List()),
        userSuggestion = userSuggestion ?? new UserSuggestionState(userSuggestions: new List());

  static AppState rehydrationJSON(dynamic json) => new AppState(
        auth: new AuthState.fromJson(json['auth']),
        userSearch: new UserSearchState.fromJSON(json['userSearch']),
        userSuggestion: new UserSuggestionState.fromJSON(json['userSuggestion']),
      );

  Map<String, dynamic> toJson() => {
        'auth': auth.toJson(),
        'userSearch': userSearch.toJSON(),
        'userSuggestion': userSuggestion.toJSON(),
      };

  AppState copyWith({
    bool rehydrated,
    AuthState auth,
    UserSearchState userSearch,
    UserSuggestionState userSuggestion,
  }) {
    return new AppState(
      auth: auth ?? this.auth,
      userSearch: userSearch ?? this.userSearch,
      userSuggestion: userSuggestion ?? this.userSuggestion,
    );
  }

  @override
  String toString() {
    return '''AppState{
            auth: $auth,
            userSearch: $userSearch,
            userSuggestion: $userSuggestion,
        }''';
  }
}

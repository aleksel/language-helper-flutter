import 'package:language_helper_flutter/models/user_suggestion.dart';
import 'package:meta/meta.dart';

@immutable
class UserSuggestionState {
  // properties
  final bool isFetched;
  final bool isLoading;
  final List<UserSuggestion> userSuggestions;
  final String errorMessage;
  final Map<String, dynamic> errorDetails;

  // constructor with default
  UserSuggestionState({
    this.isFetched = false,
    this.isLoading = false,
    this.userSuggestions,
    this.errorMessage,
    this.errorDetails,
  });

  // allows to modify UserSuggestionState parameters while cloning previous ones
  UserSuggestionState copyWith(
      {bool isFetched,
      bool isLoading,
      String errorMessage,
      Map<String, dynamic> errorDetails,
      List<UserSuggestion> userSuggestions}) {
    return new UserSuggestionState(
      isFetched: isFetched ?? this.isFetched,
      isLoading: isLoading ?? this.isLoading,
      errorMessage: errorMessage ?? this.errorMessage,
      errorDetails: errorDetails ?? this.errorDetails,
      userSuggestions: userSuggestions ?? this.userSuggestions,
    );
  }

  factory UserSuggestionState.fromJSON(Map<String, dynamic> json) {
    /*List<UserSuggestion> userSuggestions = new List<UserSuggestion>();
    if (json['userSuggestions'] != null) {
      List<dynamic> response = json['userSuggestions'];
      response.forEach((item) => userSuggestions.add(new UserSuggestion.fromJson(item)));
    }*/
    return new UserSuggestionState(
      isFetched: false,
      isLoading: false,
      errorMessage: json['errorMessage'],
      errorDetails: json['errorDetails'],
      userSuggestions: new List(),
    );
  }

  Map<String, dynamic> toJSON() => <String, dynamic>{
        'isFetched': this.isFetched,
        'isLoading': this.isLoading,
        'userSuggestions': this.userSuggestions == null ? new List() : this.userSuggestions,
        'errorMessage': this.errorMessage,
        'errorDetails': this.errorDetails,
      };

  @override
  String toString() {
    return '''{
                isFetched: $isFetched,
                isLoading: $isLoading,
                userSuggestions: $userSuggestions
                errorMessage: $errorMessage
                errorDetails: $errorDetails
            }''';
  }
}

import 'package:language_helper_flutter/models/user.dart';
import 'package:meta/meta.dart';

@immutable
class AuthState {
  // properties
  final bool isAuthenticated;
  final bool isAuthenticating;
  final User user;
  final String token;
  final String errorMessage;
  final Map<String, dynamic> errorDetails;

  // constructor with default
  AuthState({
    this.isAuthenticated = false,
    this.isAuthenticating = false,
    this.user,
    this.errorMessage,
    this.errorDetails,
    this.token,
  });

  // allows to modify AuthState parameters while cloning previous ones
  AuthState copyWith(
      {bool isAuthenticated,
      bool isAuthenticating,
      String errorMessage,
      Map<String, dynamic> errorDetails,
      User user,
      String token}) {
    return new AuthState(
      isAuthenticated: isAuthenticated ?? this.isAuthenticated,
      isAuthenticating: isAuthenticating ?? this.isAuthenticating,
      errorMessage: errorMessage ?? this.errorMessage,
      errorDetails: errorDetails ?? this.errorDetails,
      token: token ?? this.token,
      user: user ?? this.user,
    );
  }

  factory AuthState.fromJson(Map<String, dynamic> json) => new AuthState(
        isAuthenticated: json['isAuthenticated'],
        isAuthenticating: false,
        errorMessage: json['errorMessage'],
        errorDetails: json['errorDetails'],
        token: json['token'],
        user: json['user'] == null ? null : new User.fromJson(json['user']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'isAuthenticated': this.isAuthenticated,
        'isAuthenticating': this.isAuthenticating,
        'user': this.user == null ? null : this.user.toJson(),
        'token': this.token == null ? null : this.token,
        'errorMessage': this.errorMessage,
        'errorDetails': this.errorDetails,
      };

  @override
  String toString() {
    return '''{
                isAuthenticated: $isAuthenticated,
                isAuthenticating: $isAuthenticating,
                user: $user,
                token: $token
                errorMessage: $errorMessage
                errorDetails: $errorDetails
            }''';
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

class LoaderButtonContent extends StatelessWidget {
  LoaderButtonContent({
    @required this.label,
    @required this.loaderWidget,
    this.height: 40.0,
    this.isLoading: false,
    this.textStyle,
    this.loaderSize: 20.0,
  });

  final Widget loaderWidget;
  final bool isLoading;
  final String label;
  final TextStyle textStyle;
  final double loaderSize;
  final double height;
  final double defaultTextSize = 16.0;

  @override
  Widget build(BuildContext context) {
    final loaderPadding = (height - loaderSize) / 2;
    final textPadding = (height - (textStyle == null ? defaultTextSize : textStyle.fontSize)) / 2;
    return new Container(
      child: new Stack(
        children: [
          isLoading
              ? new Center(
            child: new Padding(
              padding: EdgeInsets.all(loaderPadding),
              child: new SizedBox(height: loaderSize, width: loaderSize, child: loaderWidget),
            ),
          )
              : new Container(),
          new Center(
            child: new Padding(
                padding: EdgeInsets.all(textPadding),
                child: new Text(
                  isLoading ? "" : label,
                  style: textStyle ?? new TextStyle(color: Colors.white70, fontSize: defaultTextSize),
                )),
          ),
        ],
      ),
    );
  }
}

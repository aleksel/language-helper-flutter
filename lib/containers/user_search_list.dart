import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:language_helper_flutter/models/user_search.dart';
import 'package:language_helper_flutter/styles/colors.dart';
import 'package:meta/meta.dart';
import 'package:timeago/timeago.dart';

class UserSearchList extends StatelessWidget {
  UserSearchList({@required this.getUserSearches, this.userSearches}) : assert(getUserSearches != null);

  final Function getUserSearches;
  final List<UserSearch> userSearches;

  @override
  StatelessElement createElement() {
    getUserSearches();
    return super.createElement();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new ListView.builder(
        shrinkWrap: true,
        // Let the ListView know how many items it needs to build
        itemCount: userSearches.length,
        // Provide a builder function. This is where the magic happens! We'll
        // convert each item into a Widget based on the type of item it is.
        itemBuilder: (context, index) {
          final item = userSearches[index];
          return new Container(
            width: double.infinity,
            padding: const EdgeInsets.only(top: 4.0, bottom: 4.0, left: 12.0, right: 12.0),
            color: index % 2 == 0 ? appColorStyles['primary_row'] : Colors.transparent,
            child: new Row(
              children: <Widget>[
                new Text(
                  item.searchText,
                  style: const TextStyle(fontSize: 16.0),
                ),
                new Padding(
                  padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                  child: new InputChip(
                      backgroundColor: Colors.transparent,
                      labelPadding: const EdgeInsets.all(3.0),
                      shape: new CircleBorder(side: new BorderSide(width: 0.3, color: appColorStyles['black80'])),
                      label: new Text(
                        item.count.toString(),
                        style: const TextStyle(fontSize: 12.0),
                      ),
                      onPressed: () {
                        print('I am the one thing in life.');
                      }),
                ),
                new Expanded(child: new Container()),
                new Container(
                  height: 24.0,
                  child: new InputChip(
                      backgroundColor: Colors.transparent,
                      padding: const EdgeInsets.only(bottom: 8.0),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(16.0),
                          side: new BorderSide(width: 0.3, color: appColorStyles['black80'])),
                      label: new Text(
                        timeAgo(item.updatedAt),
                        style: const TextStyle(fontSize: 12.0),
                      ),
                      onPressed: () {
                        print('I am the one thing in life.');
                      }),
                ),
              ],
            ),
            //subtitle: new Text(item.mainForm),
          );
        },
      ),
    );
  }
}

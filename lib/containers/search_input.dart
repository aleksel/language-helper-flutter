import 'package:flutter/material.dart';
import 'package:language_helper_flutter/containers/input.dart';
import 'package:language_helper_flutter/styles/colors.dart';

class SearchInput extends StatefulWidget {
  const SearchInput({this.controller, this.hintStyle, this.hintText, this.autofocus, this.onSubmitted, showLoader})
      : this.showLoader = showLoader ?? false;

  final TextEditingController controller;
  final String hintText;
  final TextStyle hintStyle;
  final bool autofocus;
  final bool showLoader;
  final Function onSubmitted;

  @override
  _SearchInputState createState() => new _SearchInputState();
}

class _SearchInputState extends State<SearchInput> {
  bool _showClearButton = false;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() => setState(() => _showClearButton = widget.controller.text.isNotEmpty));
  }

  void _onClearPressed() => widget.controller.text = '';

  @override
  Widget build(BuildContext context) {
    return new Stack(children: [
      inputInBoxTransparent(inputField(widget.controller, widget.hintText,
          hintStyle: widget.hintStyle,
          autofocus: widget.autofocus,
          onSubmitted: widget.onSubmitted,
          contentPadding: const EdgeInsets.only(left: 8.0, top: 16.0, bottom: 16.0, right: 0.0))),
      new Flex(
        children: [
          new Stack(children: [
            new IconButton(
              padding: const EdgeInsets.all(0.0),
              icon: const Icon(Icons.clear),
              onPressed: _showClearButton ? _onClearPressed : null,
              color: appColorStyles['white90'],
              disabledColor: Colors.transparent,
              //alignment: new Alignment(2.0, 0.0),
            ),
            new Container(
              padding: const EdgeInsets.all(6.0),
              child: widget.showLoader
                  ? new CircularProgressIndicator(backgroundColor: appColorStyles['gray'], strokeWidth: 2.0)
                  : new Container(
                      height: 24.0,
                    ),
            )
          ])
        ],
        mainAxisSize: MainAxisSize.max,
        direction: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.end,
      )
    ]);
  }
}

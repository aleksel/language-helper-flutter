import 'package:flutter/widgets.dart';
import 'package:language_helper_flutter/utils/system_utils.dart';

class KeyboardAware extends StatefulWidget {
  KeyboardAware({this.children, this.onAnimationStart, this.onAnimationStop});

  final List<Widget> children;
  final Function onAnimationStart;
  final Function onAnimationStop;

  @override
  _KeyboardAwareState createState() => new _KeyboardAwareState();
}

class _KeyboardAwareState extends State<KeyboardAware> with TickerProviderStateMixin {

  Animation<double> _keyboardPosition;
  AnimationController _controller;
  double bottomPadding = 0.0;
  double keyboardHeight = -0.1;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _keyboardPosition = new Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(new CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ))
      ..addListener(() {
        setState(() {
          // the state that has changed here is the animation object’s value
          bottomPadding = _keyboardPosition.value;
        });
      })
    ..addStatusListener((status) {
      switch(status) {
        case AnimationStatus.dismissed:
          if (widget.onAnimationStop != null) widget.onAnimationStop(KeyboardStatus.hiding);
          break;
        case AnimationStatus.completed:
          if (widget.onAnimationStop != null) widget.onAnimationStop(KeyboardStatus.showing);
          break;
        case AnimationStatus.forward:
          if (widget.onAnimationStart != null) widget.onAnimationStart(KeyboardStatus.showing);
          break;
        case AnimationStatus.reverse:
          if (widget.onAnimationStart != null) widget.onAnimationStart(KeyboardStatus.hiding);
          break;
      }
    });
  }

  @override
  void didUpdateWidget(KeyboardAware oldWidget) {
    super.didUpdateWidget(oldWidget);
    final _keyboardHeight = getKeyboardHeight(context);
    if (_keyboardHeight > 0) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
    if (_keyboardHeight != 0) {
      keyboardHeight = _keyboardHeight;
    }
  }

  @override
  Widget build(BuildContext context) {
    final padding = bottomPadding * keyboardHeight;
    final children = new List<Widget>.from(widget.children);
    children.add(new Padding(padding: new EdgeInsets.only(bottom: padding)));
    return new Container(
      child: new Column(
        children: children,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

enum KeyboardStatus {
  showing,
  hiding,
}

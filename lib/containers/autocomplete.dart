import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:language_helper_flutter/styles/colors.dart';
import 'package:flutter/foundation.dart';

class Autocomplete<T> extends StatefulWidget {
  Autocomplete({this.key, this.onPress, this.renderItem, items, showHintText})
      : assert(renderItem != null),
        this.showHintText = showHintText == null ? "" : showHintText,
        this.items = items == null ? new List() : items;

  final Key key;
  final List<T> items;
  final Function onPress;
  final Function<Widget>(T item, int index) renderItem;
  final String showHintText;

  @override
  AutocompleteState createState() => new AutocompleteState();
}

class AutocompleteState extends State<Autocomplete> with SingleTickerProviderStateMixin {
  void show() {
    setState(() => _showDimmer = true);
    _controller.forward();
  }

  void hide() {
    _controller.reverse();
  }

  //List<AutocompleteWord> _items;
  bool _showDimmer = false;
  AnimationController _controller;
  Animation<double> _drawerContentsOpacity;
  Animation<Offset> _drawerDetailsPosition;

  @override
  void initState() {
    super.initState();
    /*_items = new List<AutocompleteWord>.generate(
      200,
      (i) => new AutocompleteWord(word: "Sender $i", mainForm: "Message body $i"),
    );*/
    _controller = new AnimationController(vsync: this, duration: const Duration(milliseconds: 500));
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.dismissed) setState(() => _showDimmer = false);
    });
    _drawerContentsOpacity = new CurvedAnimation(
      parent: new ReverseAnimation(_controller),
      curve: Curves.fastOutSlowIn,
    );
    _drawerDetailsPosition = new Tween<Offset>(
      begin: const Offset(0.0, -1.0),
      end: Offset.zero,
    ).animate(new CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final showAutocomplete = _showDimmer && (widget.showHintText.isNotEmpty || widget.items.length != 0);
    final double _containerHeight = MediaQuery.of(context).size.height / 2.5;
    return new Stack(children: [
      new Container(
        height: double.infinity,
        width: double.infinity,
        color: showAutocomplete ? appColorStyles['black60'] : Colors.transparent,
      ),
      new SlideTransition(
        position: _drawerDetailsPosition,
        child: new FadeTransition(
          opacity: new ReverseAnimation(_drawerContentsOpacity),
          child: showAutocomplete
              ? new Container(
                  decoration:
                      new BoxDecoration(color: appColorStyles['white'], border: new Border.all(color: Colors.black54)),
                  constraints: new BoxConstraints(
                    maxHeight: _containerHeight,
                    //minHeight: 50.0,
                  ),
                  margin: const EdgeInsets.all(8.0),
                  width: double.infinity,
                  //height: double.infinity,
                  child: new ListView.builder(
                    shrinkWrap: true,
                    // Let the ListView know how many items it needs to build
                    itemCount: widget.showHintText.isNotEmpty ? 1 : widget.items.length,
                    // Provide a builder function. This is where the magic happens! We'll
                    // convert each item into a Widget based on the type of item it is.
                    itemBuilder: (context, index) {
                      if (widget.showHintText.isNotEmpty) {
                        return new Container(
                          height: _containerHeight,
                          padding: const EdgeInsets.all(10.0),
                          color: Colors.transparent,
                          child: new Center(
                              child: new Text(
                            widget.showHintText,
                            textAlign: TextAlign.center,
                            style: const TextStyle(fontSize: 24.0),
                          )),
                          //subtitle: new Text(item.mainForm),
                        );
                      }
                      final item = widget.items[index];
                      return new GestureDetector(
                        onTap: () => widget.onPress(item.word),
                        child: widget.renderItem(item, index),
                      );
                    },
                  ),
                )
              : new Container(),
        ),
      ),
    ]);
  }
}

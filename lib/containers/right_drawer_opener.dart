import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:language_helper_flutter/utils/system_utils.dart';

class RightDrawerOpener extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ScaffoldState scaffold = Scaffold.of(context, nullOk: true);
    final bool hasDrawer = scaffold?.hasDrawer ?? false;

    return new IconButton(
      icon: const Icon(Icons.menu),
      onPressed: () {
        dismissKeyboard(context);
        hasDrawer ? Scaffold.of(context).openDrawer() : null;
      },
      tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:language_helper_flutter/actions/auth_actions.dart';
import 'package:language_helper_flutter/containers/input.dart';
import 'package:language_helper_flutter/containers/loader_button.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/styles/colors.dart';
import 'package:redux/redux.dart';

class LoginForm extends StatefulWidget {
  LoginForm(this._submitAction, this._buttonLabel);

  final Function _submitAction;
  final String _buttonLabel;

  @override
  _LoginFormState createState() => new _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final formKey = new GlobalKey<FormState>();
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

  String _usernameError = "";
  String _passwordError = "";

  bool _submit() {
    bool isValid = true;
    final form = formKey.currentState;

    if (_emailController.text.isEmpty) {
      _usernameError = 'Please enter your e-mail.';
      isValid = false;
    }
    if (_passwordController.text.isEmpty) {
      _passwordError = 'Please enter your password.';
      isValid = false;
    }

    if (!isValid) setState(() {});

    if (isValid && form.validate()) {
      form.save();
    }
    return isValid;
  }

  @override
  void initState() {
    super.initState();
    _emailController.addListener(() {
      if (_emailController.text.isNotEmpty) {
        setState(() => _usernameError = "");
      }
    });

    _passwordController.addListener(() {
      if (_passwordController.text.isNotEmpty) {
        setState(() => _passwordError = "");
      }
    });
  }

  @override
  void didUpdateWidget(LoginForm oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget._buttonLabel != widget._buttonLabel) {
      _usernameError = "";
      _passwordError = "";
      _emailController.text = "";
      _passwordController.text = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, dynamic>(converter: (Store<AppState> store) {
      bool needClean = false;
      if (store.state.auth.errorDetails != null && store.state.auth.errorDetails['email'] != null) {
        _usernameError = store.state.auth.errorDetails['email'];
        needClean = true;
      } else if (store.state.auth.errorMessage != null && store.state.auth.errorMessage.isNotEmpty) {
        _usernameError = store.state.auth.errorMessage;
        needClean = true;
      }
      if (needClean) store.dispatch(clearError());
      return {
        "loginAction": (BuildContext context, String username, String password) =>
            store.dispatch(widget._submitAction(context, username, password)),
        "isAuthenticating": store.state.auth.isAuthenticating,
      };
    }, builder: (BuildContext context, storeData) {
      return new Form(
        key: formKey,
        child: new Column(
          children: [
            inputInBox(inputField(_emailController, "E-mail", obscureText: false)),
            new Padding(
              padding: const EdgeInsets.only(top: 2.0, bottom: 10.0),
              child: _usernameError.isNotEmpty
                  ? new Text(
                      _usernameError,
                      style: new TextStyle(color: Colors.redAccent),
                    )
                  : new Container(),
            ),
            inputInBox(inputField(_passwordController, "Password", obscureText: true)),
            new Padding(
              padding: const EdgeInsets.only(top: 2.0),
              child: new Text(_passwordError, style: new TextStyle(color: Colors.redAccent)),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: new Container(
                  width: double.infinity,
                  child: new RaisedButton(
                      disabledColor: appColorStyles['primary_dasabled'],
                      color: appColorStyles['primary'],
                      elevation: 0.0,
                      onPressed: storeData['isAuthenticating']
                          ? null
                          : () {
                              if (_submit()) {
                                storeData['loginAction'](context, _emailController.text, _passwordController.text);
                              }
                            },
                      child: new LoaderButtonContent(
                        loaderWidget: new CircularProgressIndicator(strokeWidth: 3.0),
                        label: widget._buttonLabel,
                        isLoading: storeData['isAuthenticating'],
                      ))),
            ),
          ],
        ),
      );
    });
  }
}

import 'package:flutter/material.dart';
import 'package:language_helper_flutter/styles/colors.dart';

final inputField = (TextEditingController controller, String hintText,
        {bool obscureText: false,
        TextStyle hintStyle,
        contentPadding: const EdgeInsets.all(12.0),
        autofocus: false,
        onSubmitted}) =>
    new TextField(
      autofocus: autofocus,
      controller: controller,
      onSubmitted: onSubmitted ?? (String searchText) => {},
      keyboardType: TextInputType.emailAddress,
      decoration: new InputDecoration(
          hintStyle: hintStyle,
          hintText: hintText,
          contentPadding: contentPadding,
          isDense: true,
          border: InputBorder.none),
      style: new TextStyle(fontSize: 18.0, color: appColorStyles['white95']),
      obscureText: obscureText,
    );

final inputInBox = (Widget widget) => new Container(
      height: 46.0,
      child: widget,
      decoration: new BoxDecoration(
          color: new Color.fromRGBO(255, 255, 255, 0.35), borderRadius: new BorderRadius.circular(4.0)),
    );

final inputInBoxTransparent = (Widget widget) => new Container(
      height: 46.0,
      child: widget,
      decoration:
          new BoxDecoration(border: new BorderDirectional(bottom: new BorderSide(color: appColorStyles['white70']))),
    );

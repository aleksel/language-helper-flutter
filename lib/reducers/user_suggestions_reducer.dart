import 'package:language_helper_flutter/actions/user_searches_actions.dart';
import 'package:language_helper_flutter/actions/user_suggestion_actions.dart';
import 'package:language_helper_flutter/models/state/user_search_state.dart';
import 'package:language_helper_flutter/models/state/user_suggestion_state.dart';
import 'package:redux/redux.dart';

Reducer<UserSuggestionState> userSuggestionReducer = combineReducers([
  new TypedReducer<UserSuggestionState, UserSuggestionRequest>(userSuggestionRequestReducer),
  new TypedReducer<UserSuggestionState, UserSuggestionSuccess>(userSuggestionSuccessReducer),
  new TypedReducer<UserSuggestionState, UserSuggestionFailure>(userSuggestionFailureReducer),
  new TypedReducer<UserSuggestionState, UserSuggestionClearError>(userSuggestionClearErrorReducer),
]);

UserSuggestionState userSuggestionRequestReducer(UserSuggestionState userSuggestion, UserSuggestionRequest action) {
  return userSuggestion.copyWith(isFetched: false, isLoading: true, userSuggestions: userSuggestion.userSuggestions);
}

UserSuggestionState userSuggestionSuccessReducer(UserSuggestionState userSuggestion, UserSuggestionSuccess action) {
  return new UserSuggestionState().copyWith(isFetched: true, isLoading: false, userSuggestions: action.userSuggestion);
}

UserSuggestionState userSuggestionFailureReducer(UserSuggestionState userSuggestion, UserSuggestionFailure action) {
  return new UserSuggestionState().copyWith(
      isFetched: false,
      isLoading: false,
      errorMessage: action.errorMessage,
      errorDetails: action.details,
      userSuggestions: userSuggestion.userSuggestions);
}

UserSuggestionState userSuggestionClearErrorReducer(
    UserSuggestionState userSuggestion, UserSuggestionClearError action) {
  return userSuggestion.copyWith(
      errorMessage: "", errorDetails: new Map<String, dynamic>(), userSuggestions: new List());
}

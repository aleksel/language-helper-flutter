import 'package:language_helper_flutter/actions/user_searches_actions.dart';
import 'package:language_helper_flutter/models/state/user_search_state.dart';
import 'package:redux/redux.dart';

Reducer<UserSearchState> userSearchReducer = combineReducers([
  new TypedReducer<UserSearchState, UserSearchesRequest>(userSearchRequestReducer),
  new TypedReducer<UserSearchState, UserSearchesSuccess>(userSearchSuccessReducer),
  new TypedReducer<UserSearchState, UserSearchesFailure>(userSearchFailureReducer),
  new TypedReducer<UserSearchState, UserSearchesClearError>(userSearchClearErrorReducer),
]);

UserSearchState userSearchRequestReducer(UserSearchState userSearch, UserSearchesRequest action) {
  return new UserSearchState().copyWith(isFetched: false, isLoading: true, userSearches: userSearch.userSearches);
}

UserSearchState userSearchSuccessReducer(UserSearchState userSearch, UserSearchesSuccess action) {
  return new UserSearchState().copyWith(isFetched: true, isLoading: false, userSearches: action.userSearches);
}


UserSearchState userSearchFailureReducer(UserSearchState userSearch, UserSearchesFailure action) {
  return new UserSearchState().copyWith(
      isFetched: false,
      isLoading: false,
      errorMessage: action.errorMessage,
      errorDetails: action.details,
      userSearches: userSearch.userSearches);
}

UserSearchState userSearchClearErrorReducer(UserSearchState userSearch, UserSearchesClearError action) {
  return userSearch.copyWith(errorMessage: "", errorDetails: new Map<String, dynamic>());
}

import 'package:language_helper_flutter/actions/auth_actions.dart';
import 'package:language_helper_flutter/models/state/auth_state.dart';
import 'package:redux/redux.dart';

Reducer<AuthState> authReducer = combineReducers([
  new TypedReducer<AuthState, UserLoginRequest>(userLoginRequestReducer),
  new TypedReducer<AuthState, UserLoginSuccess>(userLoginSuccessReducer),
  new TypedReducer<AuthState, UserLoginFailure>(userLoginFailureReducer),
  new TypedReducer<AuthState, UserLogout>(userLogoutReducer),
  new TypedReducer<AuthState, UserClearError>(userClearErrorReducer),
]);

AuthState userLoginRequestReducer(AuthState auth, UserLoginRequest action) {
  return new AuthState().copyWith(
    isAuthenticated: false,
    isAuthenticating: true,
  );
}

AuthState userLoginSuccessReducer(AuthState auth, UserLoginSuccess action) {
  return new AuthState()
      .copyWith(isAuthenticated: true, isAuthenticating: false, user: action.user, token: action.user.token);
}

AuthState userLoginFailureReducer(AuthState auth, UserLoginFailure action) {
  return new AuthState().copyWith(
      isAuthenticated: false,
      isAuthenticating: false,
      errorMessage: action.errorMessage,
      errorDetails: action.details,
      user: auth.user);
}

AuthState userLogoutReducer(AuthState auth, UserLogout action) {
  return new AuthState();
}

AuthState userClearErrorReducer(AuthState auth, UserClearError action) {
  return new AuthState().copyWith(errorMessage: "", errorDetails: new Map<String, dynamic>());
}

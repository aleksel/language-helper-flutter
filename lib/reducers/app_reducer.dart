import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/reducers/auth_reducer.dart';
import 'package:language_helper_flutter/reducers/user_searches_reducer.dart';
import 'package:language_helper_flutter/reducers/user_suggestions_reducer.dart';
import 'package:redux_persist/redux_persist.dart';

AppState appReducer(AppState state, action) {
  //print(action);
  if (action is PersistLoadedAction<AppState>) {
    return action.state ?? state;
  } else {
    return new AppState(
      auth: authReducer(state.auth, action),
      userSearch: userSearchReducer(state.userSearch, action),
      userSuggestion: userSuggestionReducer(state.userSuggestion, action),
    );
  }
}

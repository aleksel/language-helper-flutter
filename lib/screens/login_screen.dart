import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:language_helper_flutter/actions/auth_actions.dart';
import 'package:language_helper_flutter/containers/keyboard_aware.dart';
import 'package:language_helper_flutter/containers/login_form.dart';
import 'package:language_helper_flutter/styles/colors.dart';
import 'package:language_helper_flutter/utils/image_utils.dart';
import 'package:language_helper_flutter/utils/system_utils.dart';
import 'package:language_helper_flutter/utils/text_constatnt.dart';

enum CurrentForm { login, register, restorePassword }

class _BottomButton {
  _BottomButton(this.label, this.action, this.description);

  String label;
  String description;
  Function action;
}

final LoginForm _loginForm = new LoginForm(login, loginLoginButtonText);
final LoginForm _registerForm = new LoginForm(register, loginRegisterButtonText);

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  CurrentForm _currentForm = CurrentForm.login;

  var _bottomButtons = {};
  bool _showLogo = true;

  Widget _getCurrentForm(CurrentForm currentForm) {
    switch (currentForm) {
      case CurrentForm.login:
        return new Padding(padding: new EdgeInsets.symmetric(horizontal: 24.0), child: _loginForm);
      case CurrentForm.register:
        return new Padding(padding: new EdgeInsets.symmetric(horizontal: 24.0), child: _registerForm);
      case CurrentForm.restorePassword:
        print('Green as grass!');
        return new Container();
      default:
        return null;
    }
  }

  List<Widget> _getCurrentBottomButtons(CurrentForm currentForm) {
    double _width = MediaQuery.of(context).size.width;

    _BottomButton firstButton;
    _BottomButton secondButton;

    switch (currentForm) {
      case CurrentForm.login:
        firstButton = _bottomButtons[CurrentForm.register];
        secondButton = _bottomButtons[CurrentForm.restorePassword];
        break;

      case CurrentForm.register:
        firstButton = _bottomButtons[CurrentForm.login];
        secondButton = _bottomButtons[CurrentForm.restorePassword];
        break;

      case CurrentForm.restorePassword:
        firstButton = _bottomButtons[CurrentForm.login];
        secondButton = _bottomButtons[CurrentForm.register];
        break;

      default:
        return null;
    }

    return [
      new Container(
        child: new RaisedButton(
          color: appColorStyles['primary_dark'],
          elevation: 0.0,
          onPressed: firstButton.action,
          shape: new Border(),
          child: new Text(
            firstButton.label,
            style: new TextStyle(color: Colors.white70, fontSize: 14.0),
          ),
        ),
        width: _width / 2,
        height: 48.0,
      ),
      new Container(
        child: new RaisedButton(
          color: appColorStyles['primary_dark'],
          elevation: 0.0,
          onPressed: secondButton.action,
          shape: new Border(),
          child: new Text(
            secondButton.label,
            style: new TextStyle(color: Colors.white70, fontSize: 14.0),
          ),
        ),
        width: _width / 2,
        height: 48.0,
      ),
    ];
  }

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    super.initState();
    _bottomButtons = {
      CurrentForm.login: new _BottomButton(
          loginLoginBottomButton,
          () => setState(() {
                this._currentForm = CurrentForm.login;
              }),
          loginLoginDescription),
      CurrentForm.register: new _BottomButton(
          loginRegisterBottomButton,
          () => setState(() {
                this._currentForm = CurrentForm.register;
              }),
          loginRegisterDescription),
      CurrentForm.restorePassword: new _BottomButton(
        loginForgetPasswordBottomButton,
          () => setState(() {
                this._currentForm = CurrentForm.restorePassword;
              }),
          loginForgetPasswordDescription),
    };
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.black,
      body: new GestureDetector(
        onTap: () => dismissKeyboard(context),
        child: new Stack(children: [
          new Image.asset(
            ImageAssets.loginBackground,
            gaplessPlayback: true,
            alignment: Alignment.topCenter,
            height: double.infinity,
            width: double.infinity,
            fit: BoxFit.fill,
          ),
          new Container(
              child: new KeyboardAware(
                onAnimationStop: (animationStatus) {
                  if (animationStatus == KeyboardStatus.showing) setState(() => _showLogo = false);
                  else setState(() => _showLogo = true);
                },
            children: [
              new Expanded(
                child: _showLogo || getKeyboardHeight(context) == 0
                    ? new Container(
                        child: new Padding(
                          padding: new EdgeInsets.only(top: 45.0, bottom: 15.0),
                          child: new Image.asset(ImageAssets.personIcon,
                              alignment: Alignment.topCenter, height: 150.0, width: 150.0),
                        ),
                      )
                    : new Container(),
              ),
              new Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
                child: new Container(
                    width: double.infinity,
                    child: new Text(
                      loginMainText,
                      style:
                          new TextStyle(fontSize: 40.0, color: appColorStyles['white80'], fontWeight: FontWeight.bold),
                      textAlign: TextAlign.start,
                    )),
              ),
              new Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
                child: new Container(
                    width: double.infinity,
                    child: new Text(
                      _bottomButtons[_currentForm].description,
                      style: new TextStyle(fontSize: 15.0, color: appColorStyles['white50']),
                      textAlign: TextAlign.start,
                    )),
              ),
              _getCurrentForm(_currentForm),
              new Padding(
                padding: const EdgeInsets.only(top: 32.0),
                child: new Flex(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  direction: Axis.horizontal,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: _getCurrentBottomButtons(_currentForm),
                ),
              ),
            ],
          )),
        ]),
      ),
    );
  }
}

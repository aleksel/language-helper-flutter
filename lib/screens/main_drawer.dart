import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:language_helper_flutter/actions/auth_actions.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/models/user.dart';

const String _kGalleryAssetsPackage = 'flutter_gallery_assets';

class MainDrawer extends StatefulWidget {
  static const String routeName = '/material/drawer';

  @override
  _MainDrawerState createState() => new _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> with TickerProviderStateMixin {

  static const List<String> _drawerContents = const <String>[
    'A',
    'B',
    'C',
    'D',
    'E',
  ];

  AnimationController _controller;
  Animation<double> _drawerContentsOpacity;
  Animation<Offset> _drawerDetailsPosition;
  bool _showDrawerContents = true;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );
    _drawerContentsOpacity = new CurvedAnimation(
      parent: new ReverseAnimation(_controller),
      curve: Curves.fastOutSlowIn,
    );
    _drawerDetailsPosition = new Tween<Offset>(
      begin: const Offset(0.0, -1.0),
      end: Offset.zero,
    ).animate(new CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, dynamic>(
        converter: (store) {
          return {
            "logoutAction": (BuildContext context) => store.dispatch(logout(context)),
            "user": store.state.auth.user,
          };
        },
        builder: (BuildContext context, storeData) {
          final user = storeData['user'] ?? new User();
          return new Drawer(
              child: new Column(
                children: <Widget>[
                  new UserAccountsDrawerHeader(
                    accountName: new Text('${user.firstName} ${user.lastName}'),
                    accountEmail: new Text(user.email ?? ''),
                    currentAccountPicture: const CircleAvatar(
                        /*backgroundImage: const AssetImage(
                          _kAsset0,
                          package: _kGalleryAssetsPackage, // ignore: const_eval_throws_exception
                        ),*/
                        ),
                    otherAccountsPictures: <Widget>[
                      new GestureDetector(
                        onTap: () {
                          //_onOtherAccountsTap(context);
                        },
                        child: new Semantics(
                          label: 'Switch to Account B',
                          child: const CircleAvatar(
                              /*backgroundImage: const AssetImage(
                                _kAsset1,
                                package: _kGalleryAssetsPackage,
                              ),*/
                              ),
                        ),
                      ),
                      new GestureDetector(
                        onTap: () {
                          //_onOtherAccountsTap(context);
                        },
                        child: new Semantics(
                          label: 'Switch to Account C',
                          child: const CircleAvatar(
                              /* backgroundImage: const AssetImage(
                                _kAsset2,
                                package: _kGalleryAssetsPackage,
                              ),*/
                              ),
                        ),
                      ),
                    ],
                    margin: EdgeInsets.zero,
                    onDetailsPressed: () {
                      _showDrawerContents = !_showDrawerContents;
                      if (_showDrawerContents)
                        _controller.reverse();
                      else
                        _controller.forward();
                    },
                  ),
                  new MediaQuery.removePadding(
                    context: context,
                    // DrawerHeader consumes top MediaQuery padding.
                    removeTop: true,
                    child: new Expanded(
                      child: new ListView(
                        padding: const EdgeInsets.only(top: 8.0),
                        children: <Widget>[
                          new Stack(
                            children: <Widget>[
                              // The initial contents of the drawer.
                              new FadeTransition(
                                opacity: _drawerContentsOpacity,
                                child: new Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: _drawerContents.map((String id) {
                                    return new ListTile(
                                      leading: new CircleAvatar(child: new Text(id)),
                                      title: new Text('Drawer item $id'),
                                      //onTap: _showNotImplementedMessage,
                                    );
                                  }).toList(),
                                ),
                              ),
                              // The drawer's "details" view.
                              new SlideTransition(
                                position: _drawerDetailsPosition,
                                child: new FadeTransition(
                                  opacity: new ReverseAnimation(_drawerContentsOpacity),
                                  child: new Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                      new ListTile(
                                        leading: const Icon(Icons.add),
                                        title: const Text('Add account'),
                                        onTap: () => storeData['logoutAction'](context),
                                      ),
                                      new ListTile(
                                        leading: const Icon(Icons.settings),
                                        title: const Text('Manage accounts'),
                                        onTap: () {
                                          Navigator.of(context).pop();
                                          Navigator.of(context).pushNamed('/empty');
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
        });
  }
}

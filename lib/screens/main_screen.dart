import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:language_helper_flutter/containers/autocomplete.dart';
import 'package:language_helper_flutter/containers/right_drawer_opener.dart';
import 'package:language_helper_flutter/containers/user_search_list.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/models/user_suggestion.dart';
import 'package:language_helper_flutter/presentation/platform_adaptive.dart';
import 'package:language_helper_flutter/screens/main_drawer.dart';
import 'package:language_helper_flutter/screens/main_tabs/discover_tab.dart';
import 'package:language_helper_flutter/screens/main_tabs/news_tab.dart';
import 'package:language_helper_flutter/screens/main_tabs/stats_tab.dart';
import 'package:language_helper_flutter/containers/search_input.dart';
import 'package:language_helper_flutter/styles/colors.dart';
import 'package:language_helper_flutter/styles/texts.dart';
import 'package:language_helper_flutter/utils/system_utils.dart';
import 'package:language_helper_flutter/actions/user_searches_actions.dart';
import 'package:language_helper_flutter/actions/user_suggestion_actions.dart';
import 'package:language_helper_flutter/utils/text_constatnt.dart';
import 'package:redux/redux.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  @override
  State<MainScreen> createState() => new MainScreenState();
}

class MainScreenState extends State<MainScreen> with WidgetsBindingObserver {
  final TextEditingController _searchController = new TextEditingController();
  final _autocompleteKey = new GlobalKey<AutocompleteState>();
  PageController _tabController;
  bool _autocompleteShow = false;
  int _index;
  Function _onSearchChange;
  Function _onCleanSuggestions;
  String _previousSearchText = "";
  Timer _autocompleteTimer;
  int _suggestionsCount = 0;

  static final Function renderAutocompleteItem = <Widget>(item, int index) => new Container(
        padding: const EdgeInsets.all(10.0),
        color: index % 2 == 0 ? appColorStyles['primary_row'] : Colors.transparent,
        child: new Text(
          item.word,
          style: const TextStyle(fontSize: 16.0),
        ),
        //subtitle: new Text(item.mainForm),
      );

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    WidgetsBinding.instance.addObserver(this);
    _searchController.addListener(() {
      if (_searchController.text.isEmpty && _autocompleteShow) {
        _autocompleteKey.currentState.hide();
        _autocompleteShow = false;
        if (_onCleanSuggestions != null) _onCleanSuggestions();
      } else if (_searchController.text.isNotEmpty && !_autocompleteShow) {
        _autocompleteKey.currentState.show();
        _autocompleteShow = true;
      }
      if (_searchController.text.length > 1 && _onSearchChange != null) {
        final temp = _searchController.text.replaceAll(' ', '');
        if (temp != _previousSearchText) {
          if (_autocompleteTimer != null) _autocompleteTimer.cancel();
          _autocompleteTimer = new Timer(const Duration(milliseconds: 400), () {
            _autocompleteTimer = null;
            _onSearchChange(_searchController.text);
          });
        }
        _previousSearchText = temp;
      } else if (_searchController.text.length == 1) _previousSearchText = _searchController.text.trim();
      if (_searchController.text.length < 2) _onCleanSuggestions();
    });
    _tabController = new PageController();
    //_title = TabItems[0].title;
    _index = 0;
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didUpdateWidget(MainScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  didPopRoute() {
    return new Future<bool>.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, dynamic>(converter: (Store<AppState> store) {
      return {
        "getUserSearches": () => store.dispatch(getUserSearches()),
        "userSearchesIsFetched": store.state.userSearch.isFetched,
        "userSearchesIsLoading": store.state.userSearch.isLoading,
        "userSearchesError": store.state.userSearch.errorMessage,
        "userSearches": store.state.userSearch.userSearches,
        "getWordSuggestion": (String searchText) => store.dispatch(getWordSuggestion(searchText)),
        "cleanSuggestionsError": () => store.dispatch(cleanSuggestionsError()),
        "wordSuggestionsIsFetched": store.state.userSuggestion.isFetched,
        "wordSuggestionsIsLoading": store.state.userSuggestion.isLoading,
        "wordSuggestionsError": store.state.userSuggestion.errorMessage,
        "wordSuggestionsErrorValidation":
            store.state.userSuggestion.errorDetails != null && store.state.userSuggestion.errorDetails['word'] != null
                ? store.state.userSuggestion.errorDetails['word']
                : null,
        "wordSuggestions": store.state.userSuggestion.userSuggestions,
        "userSearchResult": false,
      };
    }, builder: (BuildContext context, storeData) {
      _onSearchChange = storeData['getWordSuggestion'];
      _onCleanSuggestions = storeData['cleanSuggestionsError'];
      var autocompleteError;
      if (storeData['wordSuggestionsErrorValidation'] != null) {
        autocompleteError = storeData['wordSuggestionsErrorValidation'];
      } else if (storeData['wordSuggestionsError'] != null && storeData['wordSuggestionsError'] != '') {
        autocompleteError = httpSystemError;
      }
      return new Scaffold(
        appBar: new AppBar(
            automaticallyImplyLeading: false,
            titleSpacing: 8.0,
            actions: [new RightDrawerOpener()],
            //centerTitle: false,
            title: new SearchInput(
              autofocus: true,
              onSubmitted: (String searchText) {
                if (_searchController.text.isNotEmpty) storeData['getWordSuggestion'](_searchController.text);
              },
              hintStyle: new TextStyle(color: appColorStyles['white40_func']()),
              controller: _searchController,
              showLoader: storeData['wordSuggestionsIsLoading'],
              hintText: "Search",
            )
            //new Text(_title),
            ),
        bottomNavigationBar: new PlatformAdaptiveBottomBar(
          currentIndex: _index,
          onTap: onTap,
          show: storeData['userSearchResult'],
          items: TabItems.map((TabItem item) {
            return new BottomNavigationBarItem(
              title: new Text(
                item.title,
                style: textStyles['bottom_label'],
              ),
              icon: new Icon(item.icon),
            );
          }).toList(),
        ),
        body: new GestureDetector(
          child: new Stack(
            children: [
              new Center(
                child: new Text(
                  "Enter something",
                  style: const TextStyle(fontSize: 24.0),
                ),
              ),
              new UserSearchList(
                getUserSearches: storeData['getUserSearches'],
                userSearches: storeData['userSearches'],
              ),
              storeData['userSearchResult']
                  ? new PageView(
                      controller: _tabController,
                      onPageChanged: onTabChanged,
                      children: [new NewsTab(), new StatsTab(), new DiscoverTab()],
                    )
                  : new Container(),
              new Autocomplete<UserSuggestion>(
                key: _autocompleteKey,
                showHintText: autocompleteError,
                items: storeData['wordSuggestions'],
                renderItem: renderAutocompleteItem,
                onPress: (text) => print(text),
              ),
            ],
          ),
          onTap: () => dismissKeyboard(context),
        ),
        drawer: new MainDrawer(),
      );
    });
  }

  void onTap(int tab) {
    _tabController.jumpToPage(tab);
  }

  void onTabChanged(int tab) {
    setState(() {
      this._index = tab;
    });

    //this._title = TabItems[tab].title;
  }
}

class TabItem {
  final String title;
  final IconData icon;

  const TabItem({this.title, this.icon});
}

const List<TabItem> TabItems = const <TabItem>[
  const TabItem(title: 'News', icon: Icons.assignment),
  const TabItem(title: 'Statistics', icon: Icons.timeline),
  const TabItem(title: 'Discover', icon: Icons.group_work)
];

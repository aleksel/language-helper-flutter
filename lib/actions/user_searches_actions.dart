import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jaguar_jwt/jaguar_jwt.dart';
import 'package:language_helper_flutter/middleware/api_middleware.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/models/user.dart';
import 'package:language_helper_flutter/models/user_search.dart';
import 'package:language_helper_flutter/utils/http_utils.dart';
import 'package:language_helper_flutter/utils/text_constatnt.dart';
import 'package:redux/redux.dart';

class UserSearchesRequest extends Promise {
  UserSearchesRequest(Function promise, {failureAction, successAction, errorHandler, successHandler})
      : super(promise,
            failureAction: failureAction,
            successAction: successAction,
            failureHandler: errorHandler,
            successHandler: successHandler);
}

class UserSearchesClearError {}

class UserSearchesSuccess {
  final List<UserSearch> userSearches;

  UserSearchesSuccess(this.userSearches);
}

class UserSearchesFailure extends ActionFailure {
  UserSearchesFailure(errorMessage, [details]) : super(errorMessage, details);
}

final Function clearError = () => (Store<AppState> store) => store.dispatch(new UserSearchesClearError());
final Function successAction = (store) => (data) {
      if (data.response != null) {
        List<UserSearch> userSearches = new List<UserSearch>();
        List<dynamic> response = data.response;
        response.forEach((item) => userSearches.add(new UserSearch.fromJson(item)));
        store.dispatch(new UserSearchesSuccess(userSearches));
      } else {
        store.dispatch(new UserSearchesFailure(httpSystemError));
      }
    };

final Function getUserSearches = () {
  return (Store<AppState> store) {
    store.dispatch(new UserSearchesRequest(
        () => HttpUtils.get('$API_URL$API_GET_USER_SEARCHES_PATH', store.state.auth.token),
        failureAction: failureAction(store,
            (String errorMessage, Map<String, dynamic> details) => new UserSearchesFailure(errorMessage, details)),
        successAction: successAction(store)));
  };
};

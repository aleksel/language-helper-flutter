import 'package:flutter/material.dart';
import 'package:language_helper_flutter/middleware/api_middleware.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/models/user.dart';
import 'package:language_helper_flutter/utils/http_utils.dart';
import 'package:language_helper_flutter/utils/text_constatnt.dart';
import 'package:redux/redux.dart';

final key =
    'X6YND0KxEB@q75YT%.nm>c86(HG>YV5^_90h;lWK0yt[A\$Wl-pFZ<z8&wn^\$9QtEQ_eAcv;mQxydKRW<0XQ%RP%B+Vf!Q!^zwybaXb3qGLUB,A#q%61!gkoH8zPgt\$om';

class UserLoginRequest extends Promise {
  UserLoginRequest(Function promise, {failureAction, successAction, errorHandler, successHandler})
      : super(promise,
            failureAction: failureAction,
            successAction: successAction,
            failureHandler: errorHandler,
            successHandler: successHandler);
}

class UserClearError {}

class UserLoginSuccess {
  final User user;

  UserLoginSuccess(this.user);
}

class UserLoginFailure extends ActionFailure {
  UserLoginFailure(errorMessage, [details]) : super(errorMessage, details);
}

class UserLogout {}

final Function clearError = () => (Store<AppState> store) => store.dispatch(new UserClearError());
final Function loginRegisterSuccessAction = (Store<AppState> store) => (HttpResponse data) {
      if (data.response != null && data.response['token'] != null) {
        store.dispatch(new UserLoginSuccess(new User(data.response['token'])));
      } else {
        store.dispatch(new UserLoginFailure(httpSystemError));
      }
    };

final Function register = (BuildContext context, String username, String password) {
  return (Store<AppState> store) {
    store.dispatch(new UserLoginRequest(
        () => HttpUtils
            .post('$API_URL$API_REGISTER_PATH', store.state.auth.token, {'email': username, 'password': password}),
        failureAction: failureAction(
            store, (String errorMessage, Map<String, dynamic> details) => new UserLoginFailure(errorMessage, details)),
        successAction: loginRegisterSuccessAction(store)));
  };
};
final Function login = (BuildContext context, String username, String password) {
  return (Store<AppState> store) {
    store.dispatch(new UserLoginRequest(
        () => HttpUtils
            .post('$API_URL$API_LOGIN_PATH', store.state.auth.token, {'email': username, 'password': password}),
        failureAction: failureAction(
            store, (String errorMessage, Map<String, dynamic> details) => new UserLoginFailure(errorMessage, details)),
        successAction: loginRegisterSuccessAction(store)));
  };
};

final Function logout = (BuildContext context) {
  return (Store<AppState> store) {
    store.dispatch(new UserLogout());
    Navigator.of(context).pushNamedAndRemoveUntil('/login', (_) => false);
  };
};

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jaguar_jwt/jaguar_jwt.dart';
import 'package:language_helper_flutter/middleware/api_middleware.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:language_helper_flutter/models/user.dart';
import 'package:language_helper_flutter/models/user_search.dart';
import 'package:language_helper_flutter/models/user_suggestion.dart';
import 'package:language_helper_flutter/utils/http_utils.dart';
import 'package:language_helper_flutter/utils/text_constatnt.dart';
import 'package:redux/redux.dart';

class UserSuggestionRequest extends Promise {
  UserSuggestionRequest(Function promise, {failureAction, successAction, errorHandler, successHandler})
      : super(promise,
            failureAction: failureAction,
            successAction: successAction,
            failureHandler: errorHandler,
            successHandler: successHandler);
}

class UserSuggestionClearError {}

class UserSuggestionSuccess {
  final List<UserSuggestion> userSuggestion;

  UserSuggestionSuccess(this.userSuggestion);
}

class UserSuggestionFailure extends ActionFailure {
  UserSuggestionFailure(errorMessage, [details]) : super(errorMessage, details);
}

final Function cleanSuggestionsError = () => (Store<AppState> store) => store.dispatch(new UserSuggestionClearError());
final Function successAction = (store) => (data) {
      if (data.response != null) {
        List<UserSuggestion> userSuggestions = new List<UserSuggestion>();
        List<dynamic> response = data.response;
        response.forEach((item) => userSuggestions.add(new UserSuggestion.fromJson(item)));
        store.dispatch(new UserSuggestionSuccess(userSuggestions));
      } else {
        store.dispatch(new UserSuggestionFailure(httpSystemError));
      }
    };

final Function getWordSuggestion = (String searchText) {
  return (Store<AppState> store) {
    store.dispatch(new UserSuggestionRequest(
        () => HttpUtils
            .post('$API_URL$API_GET_USER_SUGGESTIONS_PATH', store.state.auth.token, {'searchText': searchText}),
        failureAction: failureAction(store,
            (String errorMessage, Map<String, dynamic> details) => new UserSuggestionFailure(errorMessage, details)),
        successAction: successAction(store)));
  };
};

import 'dart:async';
import 'dart:convert';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:http/http.dart' as http;
import 'package:language_helper_flutter/main.dart';
import 'package:language_helper_flutter/models/state/app_state.dart';
import 'package:redux/redux.dart';

const AUTHORIZATION_HEADER = 'Authorization';
const API_URL = 'http://127.0.0.1:3000';

const API_REGISTER_PATH = '/auth/register';
const API_LOGIN_PATH = '/auth/login';

const API_GET_USER_SEARCHES_PATH = '/user/get_user_searches';

const API_GET_USER_SUGGESTIONS_PATH = '/word/get_word_suggestions';

const Map<String, String> HEADERS = const {
  'Content-Type': 'application/json',
  //'X-DEVICE-ID':               uniqueID,
  //'X-APPLICATION-INSTANCE-ID': Platform.OS === 'ios' ? uniqueID : (DeviceInfo.getInstanceID() + ''),
};

class HttpUtils {
  static Future post(String url, String token, dynamic data,
      [Map<String, String> headers = const {}, int timeout, bool json = true]) async {
    final client = new http.Client();
    final _head = getHeaders(headers, token);
    print(['POST', url, data, _head]);
    return await client
        .post(url, body: jsonEncode(data), headers: _head)
        .timeout(timeout == null ? Duration(seconds: 30) : Duration(seconds: timeout))
        .then((response) {
      print(['RESPONSE: $url', response.body]);
      return new HttpResponse(response.statusCode, jsonDecode(response.body));
    }).catchError((Object error) {
      // @TODO log error in smw
      return new Future<HttpResponse>.value(new HttpResponse(500, {}));
    }).whenComplete(client.close);
  }

  static Future get(String url, String token,
      [Map<String, String> headers = const {}, int timeout, bool json = true]) async {
    final client = new http.Client();
    final _head = getHeaders(headers, token);
    print(['GET', url, _head]);
    return await client
        .get(url, headers: _head)
        .timeout(timeout == null ? Duration(seconds: 30) : Duration(seconds: timeout))
        .then((response) {
      print(['RESPONSE: $url', response.body]);
      return new HttpResponse(response.statusCode, jsonDecode(response.body));
    }).catchError((Object error) {
      // @TODO log error in smw
      return new Future<HttpResponse>.value(new HttpResponse(500, {}));
    }).whenComplete(client.close);
  }
}

Map<String, String> getHeaders(Map<String, String> additionalHeaders, String token) {
  final headers = new Map<String, String>.from(additionalHeaders);
  headers.addAll(new Map<String, String>.from(HEADERS));
  if (token != null && token.isNotEmpty) {
    headers[AUTHORIZATION_HEADER] = 'Bearer $token';
  }
  return headers;
}

class HttpResponse {
  HttpResponse(this.statusCode, this.response);

  final int statusCode;
  final dynamic response;

  @override
  String toString() {
    return '''HttpResponse{
            statusCode: $statusCode,
            response: $response,
        }''';
  }
}

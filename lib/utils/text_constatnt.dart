// ===== SYSTEM =====
const String httpSystemError = 'Server is not available at the moment, please try later.';

// ===== LOGIN SCREEN =====
const String loginMainText = "Language\nHelper";

const String loginLoginDescription = 'please login.';
const String loginLoginButtonText = 'Log in';

const String loginRegisterDescription = 'please register.';
const String loginRegisterButtonText = 'Register';

const String loginForgetPasswordDescription = 'please fill e-mail.';
const String loginForgetPasswordButtonText = 'Reset';

const String loginLoginBottomButton = 'Log in';
const String loginRegisterBottomButton = 'Register a new account';
const String loginForgetPasswordBottomButton = 'Forget your password?';
